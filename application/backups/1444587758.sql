DROP TABLE contentlinks;

CREATE TABLE `contentlinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `linkedcontent_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `container` varchar(255) NOT NULL,
  `var1` text,
  `var2` text,
  `var3` text,
  `var4` text,
  `var5` text,
  UNIQUE KEY `id` (`id`),
  KEY `contentlinks_ibfk_1` (`content_id`),
  KEY `contentlinks_ibfk_2` (`linkedcontent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE contentmodules;

CREATE TABLE `contentmodules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `viewfolder` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

INSERT INTO contentmodules VALUES("24","Text mit Bild","Ein Text mit einem Bild, das Standardcontentelement","templates/contents/textmitbild");
INSERT INTO contentmodules VALUES("25","Collection Auszug","Damit wird ein Auszug aus einer Sammlung dargestellt nach gewissen Filter- oder Ordnungskriterien. Zb. die neuesten 3 Newsartikel aus der Newssammlung.","templates/contents/collection_ex");
INSERT INTO contentmodules VALUES("26","Kunde","Das Contentmodule f&uuml;r die Kunden, wird vorallem auf der Kundenseite verwendet","templates/contents/kunde");
INSERT INTO contentmodules VALUES("27","Arbeit","Das Contentmodul f&uuml;r die Arbeitselemente","templates/contents/arbeit");
INSERT INTO contentmodules VALUES("28","Startseite","","templates/contents/startseite");
INSERT INTO contentmodules VALUES("30","Newsartikel","Ein Artikel speziell f&uuml;r die News, das Datum ist wichtig","templates/contents/newsartikel");
INSERT INTO contentmodules VALUES("34","Tag","","templates/contents/tag");
INSERT INTO contentmodules VALUES("36","Redirector","Leitet auf eine andere URL weiter.","templates/contents/redirector");
INSERT INTO contentmodules VALUES("37","Login Box","Ein Login-Formular für das Frontend","templates/contents/loginbox");
INSERT INTO contentmodules VALUES("38","User-Info Box","Box mit Informationen über den eingeloggten Frontend-User, Logout-Link, etc.","templates/contents/userinfobox");
INSERT INTO contentmodules VALUES("39","Video","Modul für Vimeo Videos","templates/contents/video");
INSERT INTO contentmodules VALUES("40","Kapitel","Modul für Videokapitel","templates/contents/kapitel");
INSERT INTO contentmodules VALUES("41","fotostack","","templates/contents/fotostack");
INSERT INTO contentmodules VALUES("42","Schauspielvideos","Element für Links zu Schauspielvideos","templates/contents/schauspielvideolink");
INSERT INTO contentmodules VALUES("43","Daten","Modul für Spieldaten","templates/contents/dates");
INSERT INTO contentmodules VALUES("44","Daten Auszug","Auszug der Spieldaten","templates/contents/dates_ex");



DROP TABLE contents;

CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(255) NOT NULL,
  `node_id` int(11) DEFAULT NULL,
  `contentmodule_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(100) DEFAULT NULL,
  `container` varchar(255) NOT NULL,
  `type` varchar(200) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `lead` text,
  `text` text,
  `tags` text,
  `code` text,
  `url` text,
  `date` varchar(255) DEFAULT NULL,
  `var1` text,
  `var2` text,
  `var3` text,
  `var4` text,
  `var5` text NOT NULL,
  `var6` text NOT NULL,
  `var7` text NOT NULL,
  `var8` text NOT NULL,
  `var9` text NOT NULL,
  `var10` text NOT NULL,
  `var11` text NOT NULL,
  `var12` text NOT NULL,
  `var13` text NOT NULL,
  `var14` text NOT NULL,
  `var15` text NOT NULL,
  `vars` text NOT NULL,
  `position` int(11) DEFAULT NULL,
  `created_on` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`shortname`)
) ENGINE=InnoDB AUTO_INCREMENT=929 DEFAULT CHARSET=utf8;

INSERT INTO contents VALUES("896","","379","37","","1","de","content","content","Login (user: \'frontend_user\', Passwort im Admin-Bereich neu setzen)","","","","","","","","378","","","","","","","","","","","","","","","","10","2013-03-06 16:11:54","1","2013-03-07 09:15:35","1");
INSERT INTO contents VALUES("897","","379","37","","1","en","content","content","Login (user: \'frontend_user\', set new password in admin-panel)","","","","","","","","378","","","","","","","","","","","","","","","","0","2013-03-06 16:12:14","1","2013-03-07 09:15:56","1");
INSERT INTO contents VALUES("898","","378","24","","1","de","content","content","Willkommen im internen Bereich","","","","","","","","","","","","","","","","","","","","","","","","10","2013-03-06 16:15:55","1","2013-03-06 16:16:14","1");
INSERT INTO contents VALUES("899","","378","24","","1","en","content","content","Welcome to the restricted area","","","","","","","","","","","","","","","","","","","","","","","","0","2013-03-06 16:16:14","1","2013-03-06 16:16:25","1");
INSERT INTO contents VALUES("900","","378","38","","1","en","footer","content","","","","","","","","","379","","","","","","","","","","","","","","","","10","2013-03-06 16:16:29","1","2013-03-06 16:16:43","1");
INSERT INTO contents VALUES("901","","378","38","","1","de","footer","content","","","","","","","","","379","","","","","","","","","","","","","","","","0","2013-03-06 16:16:43","1","2013-03-06 16:16:50","1");
INSERT INTO contents VALUES("902","","340","24","","1","de","content","content","und: inhalt","","","<p>Das Stück «Kindergeschichten» wurde im Jahre 2003 von dem Schauspieler Manuel Löwensberg und dem Regisseur Peter Schweiger am Stadttheater St. Gallen erarbeitet und uraufgeführt.<br>hier kommt ein text mit einem <a href=\"http://example.com/\" target=\"_blank\">link</a></p><p>Nach dem grossen Erfolg und nach über 90 Vorstellungen in Theatern, Schulen, Klassenzimmern, Aulen und Turnhallen des Kantons St. Gallen erlebt das Stück nun seine Wiederaufnahme und ist endlich auch in der übrigen Schweiz zu sehen.</p><p>«Kindergeschichten» ist zwar vornehmlich für Kinder konzipiert, doch auch reine «Erwachsenenvorstellungen» haben sich als äusserst spannend erwiesen, denn dank der Vielschichtigkeit des Textes und der Inszenierung kommen Kinder und Erwachsene gleichermaßen auf ihren Genuss. meine Safaritrophäen.</p><p>Das Stück «Kindergeschichten» wurde im Jahre 2003 von dem Schauspieler Manuel Löwensberg und dem Regisseur Peter Schweiger am Stadttheater St. Gallen erarbeitet und uraufgeführt.</p><p>Nach dem grossen Erfolg und nach über 90 Vorstellungen in Theatern, Schulen, Klassenzimmern, Aulen und Turnhallen des Kantons St. Gallen erlebt das Stück nun seine Wiederaufnahme und ist endlich auch in der übrigen Schweiz zu sehen.</p><p><span></span>«Kindergeschichten» ist zwar vornehmlich für Kinder konzipiert, doch auch reine «Erwachsenenvorstellungen» haben sich als äusserst spannend erwiesen, denn dank der Vielschichtigkeit des Textes und der Inszenierung kommen Kinder und Erwachsene gleichermaßen auf ihren Genuss. meine Safaritrophäen.</p><p>Das Stück «Kindergeschichten» wurde im Jahre 2003 von dem Schauspieler Manuel Löwensberg und dem Regisseur Peter Schweiger am Stadttheater St. Gallen erarbeitet und uraufgeführt.</p><p>Nach dem grossen Erfolg und nach über 90 Vorstellungen in Theatern, Schulen, Klassenzimmern, Aulen und Turnhallen des Kantons St. Gallen erlebt das Stück nun seine Wiederaufnahme und ist endlich auch in der übrigen Schweiz zu sehen.</p><p><span></span>«Kindergeschichten» ist zwar vornehmlich für Kinder konzipiert, doch auch reine «Erwachsenenvorstellungen» haben sich als äusserst spannend erwiesen, denn dank der Vielschichtigkeit des Textes und der Inszenierung kommen Kinder und Erwachsene gleichermaßen auf ihren Genuss. meine Safaritrophäen.</p>","","","","","","","","","","","","","","","","","","","","","20","2015-08-20 11:00:31","1","2015-10-05 17:33:45","1");
INSERT INTO contents VALUES("903","","340","24","","1","de","content","content","jaja, inhalt","","","<p>Hier kann noch was in normal stehen</p><ul><li>Hier so eine Aufzählung</li><li>2012 Giles Foreman Center for Acting, London </li><li>1997–2001 Hochschule für Musik und Theater Bern, Bereich Theater </li><li>1996 – 1997 Ecole Internationale de Théâtre «Lassaad», Brüssel </li><li>1996 Matura, Gymnasium Zürich</li></ul>","","","","","","","","","","","","","","","","","","","","","10","2015-08-20 11:00:32","1","2015-10-05 17:33:33","1");
INSERT INTO contents VALUES("904","","340","24","","1","de","content","content","Titel?","","<p>zuoberst: eventuell Einführungstext (kurzer Lebenslauf in Worten)??<br>dann 1 PDF ausführlich und 1 PDF zusammengefasst, (mit Fotos drin und Kontaktangaben)<span></span></p>","<p>hier kommt ein text mit einem <a href=\"http://example.com\" target=\"_blank\">link</a></p>","","","","","","","","","","","","","","","","","","","","","0","2015-08-20 11:00:32","1","2015-09-29 10:46:26","1");
INSERT INTO contents VALUES("905","","363","27","","1","de","arbeiten","content","Set Fotos","","","","","","","","","","","","","","","","","","","","","","","","10","2015-08-20 14:57:41","1","2015-10-05 10:45:01","1");
INSERT INTO contents VALUES("906","","358","25","","1","de","content","content","","","","","","","","","363","arbeiten","","","","","","","","","","","","","","","0","2015-08-20 15:00:12","1","2015-08-20 15:00:18","1");
INSERT INTO contents VALUES("908","","357","39","","1","de","content","content","Showreel 2015","","","<iframe src=\"https://player.vimeo.com/video/134140828?color=e8d3b6&title=0&byline=0&portrait=0\" width=\"500\" height=\"281\" frameborder=\"0\" webkitallowfullscreen=\"\" mozallowfullscreen=\"\" allowfullscreen=\"\"></iframe>","","","","","<p>Hier kann noch ein Text hin</p>","","","","","","","","","","","","","","","","10","2015-09-14 17:55:01","1","2015-10-06 11:59:50","1");
INSERT INTO contents VALUES("909","","","40","908","1","de","contentcontainer","content","Film soundos","","","","","","","","10","","","","","","","","","","","","","","","","40","2015-09-14 17:58:37","1","2015-10-05 15:46:30","1");
INSERT INTO contents VALUES("910","","","40","908","1","de","contentcontainer","content","Kapitel 5","","","","","","","","50","","","","","","","","","","","","","","","","30","2015-09-14 18:24:41","1","2015-10-05 15:46:30","1");
INSERT INTO contents VALUES("911","","","40","908","1","de","contentcontainer","content","0:20 - Szene 03, aus dem Film ","","","","","","","","20","","","","","","","","","","","","","","","","20","2015-09-14 18:24:41","1","2015-10-05 15:46:30","1");
INSERT INTO contents VALUES("912","","384","41","","0","de","content","content","","","","","","","","","","","","","","","","","","","","","","","","","10","2015-09-25 15:26:57","1","2015-10-05 10:49:40","1");
INSERT INTO contents VALUES("913","","384","41","","1","de","content","content","","","","","","","","","","","","","","","","","","","","","","","","","0","2015-09-25 16:24:14","1","2015-10-05 10:49:38","1");
INSERT INTO contents VALUES("914","","363","27","","1","de","arbeiten","content","Theater","","","","","","","","","","","","","","","","","","","","","","","","0","2015-10-05 10:45:01","1","2015-10-05 10:45:08","1");
INSERT INTO contents VALUES("916","","357","42","","1","de","content","content","Showreel","","","<p>http://www.schauspielervideos.de/video/manuel-loewensberg?vi=56405<br></p>","","","media/upload/videostills/Bildschirmfoto%202015-10-05%20um%2015.47.19.png","","http://www.schauspielervideos.de/video/manuel-loewensberg?vi=56405","","","","","","","","","","","","","","","","20","2015-10-05 15:46:26","1","2015-10-06 11:59:50","1");
INSERT INTO contents VALUES("917","","","","","1","","","","","","","","","","","","","","","","","","","","","","","","","","","","0","2015-10-05 15:46:30","1","","1");
INSERT INTO contents VALUES("918","","357","39","","1","de","content","content","Showreel 2015","","","<iframe width=\"500\" height=\"281\" src=\"http://www.schauspielervideos.de/video-embed/manuel-loewensberg?vi=56405&width=500&height=281&autoplay=false\" frameborder=\"0\" scrolling=\"no\" allowfullscreen=\"\"></iframe>","","","","","<p>hier kann auch was stehen</p>","","","","","","","","","","","","","","","","0","2015-10-06 11:59:50","1","2015-10-06 12:04:28","1");
INSERT INTO contents VALUES("919","","381","44","","1","de","content","content","","","","","","","","","387","daten","","","","","","","","","","","","","","","0","2015-10-11 19:34:49","1","2015-10-11 20:05:11","1");
INSERT INTO contents VALUES("926","","387","43","","1","de","daten","content","Gatsby","","","<p>Ich spiele in Aarau: <a href=\"http://theatermarie.ch\" target=\"_blank\">http://theatermarie.ch</a></p>","","","","2015-10-16","","","","","","","","","","","","","","","","","20","2015-10-11 19:58:36","1","2015-10-11 20:19:19","1");
INSERT INTO contents VALUES("927","","387","43","","1","de","daten","content","Sonst was","","","<p>Hier könnte auch eine Byline stehen</p>","","","","2015-10-22","","","","","","","","","","","","","","","","","10","2015-10-11 19:58:36","1","2015-10-11 20:19:34","1");
INSERT INTO contents VALUES("928","","387","43","","1","de","daten","content","Noch ein Datum","","","","","","","2015-10-31","","","","","","","","","","","","","","","","","0","2015-10-11 19:58:37","1","2015-10-11 20:01:39","1");



DROP TABLE medialinks;

CREATE TABLE `medialinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `container` varchar(255) NOT NULL,
  `url` varchar(512) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `mimetype` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `text` text,
  `alttag` varchar(255) NOT NULL,
  `media_parameters` text,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`,`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

INSERT INTO medialinks VALUES("1","905","1","media","/media/upload/Set/_MG_7251.jpg","_MG_7251.jpg","image/jpeg","image","jpg","","","","","110");
INSERT INTO medialinks VALUES("2","905","1","media","/media/upload/Set/_MG_7648%20-%20Arbeitskopie%202.jpg","_MG_7648%20-%20Arbeitskopie%202.jpg","image/jpeg","image","jpg","","","","","100");
INSERT INTO medialinks VALUES("3","905","1","media","/media/upload/Set/_MG_7251.jpg","_MG_7251.jpg","image/jpeg","image","jpg","","","","","90");
INSERT INTO medialinks VALUES("4","905","1","media","/media/upload/Set/_MG_7648%20-%20Arbeitskopie%202.jpg","_MG_7648%20-%20Arbeitskopie%202.jpg","image/jpeg","image","jpg","","","","","80");
INSERT INTO medialinks VALUES("5","905","1","media","/media/upload/Set/_MG_7648.jpg","_MG_7648.jpg","image/jpeg","image","jpg","","","","","70");
INSERT INTO medialinks VALUES("6","905","1","media","/media/upload/Set/_MG_7693.jpg","_MG_7693.jpg","image/jpeg","image","jpg","","","","","60");
INSERT INTO medialinks VALUES("7","905","1","media","/media/upload/Set/_MG_7779.jpg","_MG_7779.jpg","image/jpeg","image","jpg","","","","","50");
INSERT INTO medialinks VALUES("8","905","1","media","/media/upload/Set/_MG_7967_sw.jpg","_MG_7967_sw.jpg","image/jpeg","image","jpg","","","","","40");
INSERT INTO medialinks VALUES("9","905","1","media","/media/upload/Set/_MG_7967.jpg","_MG_7967.jpg","image/jpeg","image","jpg","","","","","30");
INSERT INTO medialinks VALUES("10","905","1","media","/media/upload/Set/_MG_7976.jpg","_MG_7976.jpg","image/jpeg","image","jpg","","","","","20");
INSERT INTO medialinks VALUES("11","905","1","media","/media/upload/Set/_MG_8226_sw.jpg","_MG_8226_sw.jpg","image/jpeg","image","jpg","","","","","10");
INSERT INTO medialinks VALUES("12","905","1","media","/media/upload/Set/_MG_8226.jpg","_MG_8226.jpg","image/jpeg","image","jpg","","","","","0");
INSERT INTO medialinks VALUES("19","912","1","media","/media/upload/start/sort_0000_DSC09883.png","sort_0000_DSC09883.png","image/png","image","png","","","","","70");
INSERT INTO medialinks VALUES("20","912","1","media","/media/upload/start/sort_0001_DSC09655.png","sort_0001_DSC09655.png","image/png","image","png","","","","","60");
INSERT INTO medialinks VALUES("21","912","1","media","/media/upload/start/sort_0002__MG_7693.png","sort_0002__MG_7693.png","image/png","image","png","","","","","50");
INSERT INTO medialinks VALUES("22","912","1","media","/media/upload/start/sort_0003_709-383xj-KB-Manuel-Lo%CC%88wensberg.png","sort_0003_709-383xj-KB-Manuel-Lo%CC%88wensberg.png","image/png","image","png","","","","","40");
INSERT INTO medialinks VALUES("23","912","1","media","/media/upload/start/sort_0004_709-229xj-KB-Manuel-Lo%CC%88wensberg.png","sort_0004_709-229xj-KB-Manuel-Lo%CC%88wensberg.png","image/png","image","png","","","","","30");
INSERT INTO medialinks VALUES("24","912","1","media","/media/upload/start/sort_0007_709-102xj-KB-Manuel-Lo%CC%88wensberg.png","sort_0007_709-102xj-KB-Manuel-Lo%CC%88wensberg.png","image/png","image","png","","","","","20");
INSERT INTO medialinks VALUES("25","912","1","media","/media/upload/start/sort_0005_709-451x-KB-Manuel-Lo%CC%88wensberg.png","sort_0005_709-451x-KB-Manuel-Lo%CC%88wensberg.png","image/png","image","png","","","","","10");
INSERT INTO medialinks VALUES("26","912","1","media","/media/upload/start/sort_0006_Ebene-0.png","sort_0006_Ebene-0.png","image/png","image","png","","","","","0");
INSERT INTO medialinks VALUES("27","913","1","media","/media/upload/start/portraits/portrait_0003_DSC09663.png","portrait_0003_DSC09663.png","image/png","image","png","","","","","40");
INSERT INTO medialinks VALUES("28","913","1","media","/media/upload/start/portraits/portrait_0002_DSC09759.png","portrait_0002_DSC09759.png","image/png","image","png","","","","","30");
INSERT INTO medialinks VALUES("29","913","1","media","/media/upload/start/portraits/portrait_0004_DSC09645.png","portrait_0004_DSC09645.png","image/png","image","png","","","","","20");
INSERT INTO medialinks VALUES("32","904","1","file","/media/upload/Vita%20PDF/ausfu%CC%88hrlich.pdf","ausfu%CC%88hrlich.pdf","application/pdf","application","pdf","Vita","Ausführlich","","","10");
INSERT INTO medialinks VALUES("33","904","1","file","/media/upload/Vita%20PDF/Kurzfassung.pdf","Kurzfassung.pdf","application/pdf","application","pdf","Vita","Kurzfassung","","","0");
INSERT INTO medialinks VALUES("34","914","1","media","/media/upload/Set/_MG_7251.jpg","_MG_7251.jpg","image/jpeg","image","jpg","","","","","40");
INSERT INTO medialinks VALUES("35","914","1","media","/media/upload/Set/_MG_8347.jpg","_MG_8347.jpg","image/jpeg","image","jpg","","","","","30");
INSERT INTO medialinks VALUES("36","914","1","media","/media/upload/Set/_MG_7779.jpg","_MG_7779.jpg","image/jpeg","image","jpg","","","","","20");
INSERT INTO medialinks VALUES("37","914","1","media","/media/upload/Set/_MG_8226.jpg","_MG_8226.jpg","image/jpeg","image","jpg","","","","","10");
INSERT INTO medialinks VALUES("38","914","1","media","/media/upload/Set/_MG_8542s.jpg","_MG_8542s.jpg","image/jpeg","image","jpg","","","","","0");
INSERT INTO medialinks VALUES("39","903","1","image","/media/upload/Set/_MG_7648%20-%20Arbeitskopie%202.jpg","_MG_7648%20-%20Arbeitskopie%202.jpg","image/jpeg","image","jpg","","","","","20");
INSERT INTO medialinks VALUES("40","903","1","image","/media/upload/Set/_MG_7648.jpg","_MG_7648.jpg","image/jpeg","image","jpg","","","","","10");
INSERT INTO medialinks VALUES("41","903","1","image","/media/upload/Set/_MG_7779.jpg","_MG_7779.jpg","image/jpeg","image","jpg","","","","","0");



DROP TABLE nodemetaobjects;

CREATE TABLE `nodemetaobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) DEFAULT NULL,
  `lang` varchar(200) DEFAULT NULL,
  `menutitle` varchar(200) DEFAULT NULL,
  `windowtitle` varchar(200) DEFAULT NULL,
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `var1` text NOT NULL,
  `var2` text NOT NULL,
  `var3` text NOT NULL,
  `var4` text NOT NULL,
  `var5` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;

INSERT INTO nodemetaobjects VALUES("185","340","de","Vita","Window title","description","some words, go here, yes.","","","","","");
INSERT INTO nodemetaobjects VALUES("194","340","en","Home (en)","Home (en)","","","","","","","");
INSERT INTO nodemetaobjects VALUES("195","262","en","Main Menu","Main Menu","","","","","","","");
INSERT INTO nodemetaobjects VALUES("196","262","de","Hauptmenü","Hauptmenü","","","","","","","");
INSERT INTO nodemetaobjects VALUES("204","351","de","Footermenü","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("208","351","en","Footer Menu","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("213","355","de","Disclaimer","page_355","","","","","","","");
INSERT INTO nodemetaobjects VALUES("214","356","de","Impressum","page_356","","","","","","","");
INSERT INTO nodemetaobjects VALUES("215","357","de","Video","Kontakt","","","","","","","");
INSERT INTO nodemetaobjects VALUES("217","357","en","Contact","page_357","","","","","","","");
INSERT INTO nodemetaobjects VALUES("218","356","en","Impressum","page_356","","","","","","","");
INSERT INTO nodemetaobjects VALUES("219","355","en","Disclaimer","page_355","","","","","","","");
INSERT INTO nodemetaobjects VALUES("220","358","de","Fotos","page_358","","","","","","","");
INSERT INTO nodemetaobjects VALUES("222","358","en","Work","Work","","","","","","","");
INSERT INTO nodemetaobjects VALUES("229","363","de","Collection","collection_363","","","","","","","");
INSERT INTO nodemetaobjects VALUES("230","363","en","Collection","Collection_Tables","","","","","","","");
INSERT INTO nodemetaobjects VALUES("244","375","de","Home","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("249","375","en","Home","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("257","378","en","Restricted Area","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("258","378","de","Interner Bereich","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("259","379","de","Login","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("260","379","en","Login","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("263","","en","","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("264","","de","","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("265","380","de","Presse","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("266","381","de","Termine","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("267","382","de","Kindergeschichten","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("268","383","de","Links","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("269","384","de","home","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("272","387","de","Daten","","","","","","","","");



DROP TABLE nodes;

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `scope` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `nodetemplate_id` int(11) DEFAULT NULL,
  `shortname` varchar(255) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(255) DEFAULT NULL,
  `displayhook` varchar(255) DEFAULT NULL,
  `title` text,
  `loginpage` text,
  `created_on` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`shortname`)
) ENGINE=InnoDB AUTO_INCREMENT=389 DEFAULT CHARSET=utf8;

INSERT INTO nodes VALUES("262","0","1","26","1","1","10","4","_262","1","navitree","mainnavi","Hauptmenü","","0000-00-00 00:00:00","0","2015-10-11 19:58:15","1");
INSERT INTO nodes VALUES("340","262","4","5","2","1","0","1","home","1","page","","Homepage","","0000-00-00 00:00:00","0","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("351","0","1","8","1","3","20","4","_351","1","navitree","footernavi","footernavi","","0000-00-00 00:00:00","0","2015-10-11 19:58:15","1");
INSERT INTO nodes VALUES("355","351","4","5","2","3","0","1","disclaimer","1","page","","page_355","","","0","2012-09-13 15:33:30","0");
INSERT INTO nodes VALUES("356","351","2","3","2","3","0","1","Impressum","1","page","","page_356","","","0","2012-09-22 14:38:18","1");
INSERT INTO nodes VALUES("357","262","8","9","2","1","0","1","kontakt","1","page","","page_357","","","0","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("358","262","6","7","2","1","0","1","arbeiten","1","page","","page_358","","","0","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("363","0","1","2","1","5","30","14","collection363","1","collection","","Collection","","","0","2015-10-11 19:58:15","1");
INSERT INTO nodes VALUES("375","351","6","7","2","3","0","15","home2","1","page","","page_375","","","0","2012-09-22 19:11:56","1");
INSERT INTO nodes VALUES("378","262","18","19","2","1","0","1","intern","0","page","","page_378","379","2013-03-06 16:09:20","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("379","262","20","21","2","1","0","1","intern_login","0","page","","page_379","","2013-03-06 16:10:08","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("380","262","10","11","2","1","0","1","page380","1","page","","page_380","","2015-08-20 14:55:41","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("381","262","12","13","2","1","0","1","page381","1","page","","page_381","","2015-08-20 14:55:42","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("382","262","14","15","2","1","0","1","page382","1","page","","page_382","","2015-08-20 14:56:00","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("383","262","16","17","2","1","0","1","page383","1","page","","page_383","","2015-08-20 14:56:00","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("384","262","2","3","2","1","0","1","hom","0","page","startpage","page_384","","2015-09-25 15:25:43","","2015-09-29 09:30:05","1");
INSERT INTO nodes VALUES("387","","1","2","1","6","40","16","collection387","1","collection","","daten","","2015-10-11 19:58:12","","2015-10-11 19:58:27","1");



DROP TABLE nodetemplates;

CREATE TABLE `nodetemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `type` varchar(255) NOT NULL,
  `containers` text NOT NULL,
  `viewfolder` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO nodetemplates VALUES("1","Eine Standardseite","Das Standardtemplate, welches normalerweise angewendet werden sollte.","page","content,footer","templates/pages/standard");
INSERT INTO nodetemplates VALUES("2","Collection Arbeiten /  Kunden","Das Template zur Darstellung von Inhalten, die direkt aus einer Collection gewonnen werden.","collection","arbeiten,kunden,tags","templates/collections/standard");
INSERT INTO nodetemplates VALUES("4","Navigationsbaum","Der Normale Navigationsbaum","navitree","","");
INSERT INTO nodetemplates VALUES("14","Collection DataTables","","collection","arbeiten,kunden,tags","templates/collections/tables");
INSERT INTO nodetemplates VALUES("15","Weiterleitung","<p>Pages mit diesem Template haben keinen Inhalt, sondern leiten einfach auf eine andere Page weiter.</p>","page","redirect","templates/pages/redirect");
INSERT INTO nodetemplates VALUES("16","Daten","","collection","daten","templates/collections/tables");



DROP TABLE nodetemplates_contentmodules;

CREATE TABLE `nodetemplates_contentmodules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nodetemplate_id` int(11) NOT NULL,
  `contentmodule_id` int(11) NOT NULL,
  `container` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_nodetemplate_id` (`nodetemplate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=713 DEFAULT CHARSET=utf8;

INSERT INTO nodetemplates_contentmodules VALUES("661","2","27","arbeiten");
INSERT INTO nodetemplates_contentmodules VALUES("662","2","26","kunden");
INSERT INTO nodetemplates_contentmodules VALUES("665","2","34","tags");
INSERT INTO nodetemplates_contentmodules VALUES("670","15","36","redirect");
INSERT INTO nodetemplates_contentmodules VALUES("671","14","26","kunden");
INSERT INTO nodetemplates_contentmodules VALUES("672","14","34","tags");
INSERT INTO nodetemplates_contentmodules VALUES("674","14","27","arbeiten");
INSERT INTO nodetemplates_contentmodules VALUES("678","1","37","footer");
INSERT INTO nodetemplates_contentmodules VALUES("679","1","24","footer");
INSERT INTO nodetemplates_contentmodules VALUES("680","1","38","footer");
INSERT INTO nodetemplates_contentmodules VALUES("704","16","43","Daten");
INSERT INTO nodetemplates_contentmodules VALUES("705","1","24","content");
INSERT INTO nodetemplates_contentmodules VALUES("706","1","25","content");
INSERT INTO nodetemplates_contentmodules VALUES("707","1","37","content");
INSERT INTO nodetemplates_contentmodules VALUES("708","1","38","content");
INSERT INTO nodetemplates_contentmodules VALUES("709","1","39","content");
INSERT INTO nodetemplates_contentmodules VALUES("710","1","41","content");
INSERT INTO nodetemplates_contentmodules VALUES("711","1","42","content");
INSERT INTO nodetemplates_contentmodules VALUES("712","1","44","content");



DROP TABLE roles;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO roles VALUES("1","login","Darf sich einloggen");
INSERT INTO roles VALUES("2","admin","Hauptadministrator, darf alles");
INSERT INTO roles VALUES("3","contentadmin","Kann Contents bearbeiten, aber nicht die Struktur der Seite verÃƒÂ¤ndern");
INSERT INTO roles VALUES("4","frontend-login","Beispiel-Rolle für den Frontend-Login");
INSERT INTO roles VALUES("5","backend-login","Rolle für User, die sich im Backend anmelden dürfen. (ausser User \'admin\', dieser darf sich sowieso im Backend anmelden)");



DROP TABLE roles_nodes;

CREATE TABLE `roles_nodes` (
  `node_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO roles_nodes VALUES("378","4");



DROP TABLE roles_users;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO roles_users VALUES("1","1");
INSERT INTO roles_users VALUES("2","1");
INSERT INTO roles_users VALUES("3","1");
INSERT INTO roles_users VALUES("1","2");
INSERT INTO roles_users VALUES("2","3");
INSERT INTO roles_users VALUES("3","4");



DROP TABLE user_tokens;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `type` varchar(100) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO user_tokens VALUES("1","1","4050306420e2698e2dd7e01c617c996a9b5b0977","ecb4a8fdcfb73162a7bed8738e003856d6fd15d7","","1440603898","1440690298");
INSERT INTO user_tokens VALUES("2","1","4050306420e2698e2dd7e01c617c996a9b5b0977","2a8abbaa315e176a2c3f1db1ab256715d41c2818","","1443511787","1443598187");
INSERT INTO user_tokens VALUES("3","1","79c729069b5822ba90d3714087b00b1f61ffd08e","c3dea48b2c23c9d4eaa664a420e57197cf007cc8","","1444584776","1444671176");



DROP TABLE users;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `hasglobalrights` tinyint(1) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO users VALUES("1","contact@michaelflueckiger.ch","admin","65aa6fa3a57dd507b5ead0f03ed6c4291dcc057dbaec883cc887706c7871b8e3","1","262","1444584776");
INSERT INTO users VALUES("2","b.rufer@robandrose.ch","client","b25464e5ce6acea5471c3e03b8a2409d3167e950507dc6a80f7c993cad23cbdc","1","12","1329050850");
INSERT INTO users VALUES("3","the_password_is@user123456.com","frontend_user","c1db7a1266642814f2f8e2515440ed604e3ad9d94ba1c082082daa9d4c2396dc","0","13","1362644214");



DROP TABLE users_nodes;

CREATE TABLE `users_nodes` (
  `user_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




