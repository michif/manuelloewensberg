<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Dies ist der Frontpage Controller, Verantwortlich für das Rendering der Page
 * 
 * @package    sigi
 * @author     Matthias Rohrbach
 * @copyright  (c) Matthias Rohrbach
 * @license    http://sigi.ch/license
 */
class Controller_Frontend_Frontend extends Controller_Frontend_Frontendbase
{
	public function before() {
		parent::before();
		
		// Your Settings here
		
	}
}