<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => 'naeK8aawiY7ies7AeQuoh6pee2uiw1ie',
	'lifetime'     => Date::HOUR*24,
	'session_key'  => 'auth_user',
);
