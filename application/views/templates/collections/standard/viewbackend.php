<?

echo '<div class="tabs">';
echo '<ul class="tablinks">';
foreach($model->nodetemplate->get_containers() as $container){
	echo '<li>'.HTML::anchor("#".$container."_tab", $container).'</li>';	
}	
echo '</ul>';

foreach($model->nodetemplate->get_containers() as $container){
	echo '<div id="'.$container.'_tab" class="addmenucontainer tabcontainer" >';
	$modules=$model->nodetemplate->contentmodules->where('container', '=', $container)->find_all();
	echo Backend_Addmenu::content($model->id, $container, $modules);
	
	echo '<div class="container_content">';
	echo '<ul class="sortable content elmcontainer" elmid="'.$model->id.'"  url="'.URL::base().'admin/content/"  elmclass="content_elm " >';
	
	foreach($model->contents->order_by('position')->where('container','=',$container)->where('lang','=',Session::instance()->get('sigi_lang'))->find_all() as $cont){
		$subview=new View("backend/content/container_single");
		$subview->model=$cont;
		echo $subview;		
	}
	
	echo '</ul>';
	echo '</div>';
	echo '</div>';
}
echo '</div>';// Tabs
?>

