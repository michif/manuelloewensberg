<?php

// TODO: translate to english
// Diese Properties werden für die Definition der Tabellen je Container benötigt
// Dieses File kann überschrieben werden um die Tabellendefinitionen anzupassen

return array(
		"arbeiten"	=>	array(
			"aoColumns"		=> 	array(
									array("mData" => "id", "sContentPadding"=> "0"),
									array("mData" => "title"),
									array("mData" => "subtitle"),
									array("mData" => "date"),
									array("mData" => "url"),
									array("mData" => "id")
								),
			"aoColumnDefs"	=>	array(
									array("sTitle" => "ID",			"aTargets"=>array(0)),
									array("sTitle" => "Titel",		"aTargets"=>array(1)),
									array("sTitle" => "subtitle",	"aTargets"=>array(2)),
									array("sTitle" => "date",		"aTargets"=>array(3)),
									array("sTitle" => "img",		"aTargets"=>array(4), "render"=>"function(data, type, row ){
										if(data==undefined)return \"(kein Bild)\";
										return '<img src=\"".url::base()."/imagefly/h15/'+data+'\" />';}", "bSortable"=>false),									
										// Function for action column, expecting id as data source;
									
									array(
										"sTitle" => "Del",		
										"aTargets"=>array(5), 
										"render"=>"function(data, type, full, meta ) {return '".Backend_Actionbox::tableaction()."';}", 
										"bSortable"=>false, 
										"bSearchable"=>false
										),									
									
								)
		),
		 "daten"   =>  array(
         
            "aoColumns"     =>  array(
                                    array("mData" => "id", "sContentPadding"=> "0"),
                                    array("mData" => "title"),
                                    array("mData" => "subtitle"),
                                    array("mData" => "date"),
                                    array("mData" => "url"),
                                    array("mData" => "id")
                                ),
            "aoColumnDefs"  =>  array(
                                    array("sTitle" => "ID",         "aTargets"=>array(0)),
                                    array("sTitle" => "Titel",      "aTargets"=>array(1)),
                                    array("sTitle" => "subtitle",   "aTargets"=>array(2)),
                                    array("sTitle" => "date",       "aTargets"=>array(3)),
                                    array("sTitle" => "img",        "aTargets"=>array(4), "render"=>"function(data, type, row ){
                                        if(data==undefined)return \"(kein Bild)\";
                                        return '<img src=\"".url::base()."/imagefly/h15/'+data+'\" />';}", "bSortable"=>false),                                 
                                        // Function for action column, expecting id as data source;
                                    
                                    array(
                                        "sTitle" => "Del",      
                                        "aTargets"=>array(5), 
                                        "render"=>"function(data, type, full, meta ) {return '".Backend_Actionbox::tableaction()."';}", 
                                        "bSortable"=>false, 
                                        "bSearchable"=>false
                                        ),                                  
                                    
                                )
        ),

		"kunden"	=>	array(
			"aoColumns"		=>	array(
									array("mData" => "id"),
									array("mData" => "title"),
								),
			"aoColumnDefs"	=>	array(
									array("sTitle" => "ID",		"aTargets" => array(0)),
									array("sTitle" => "Titel",	"aTargets" => array(1)),
								)
		),


		"tags"=>array(
			"aoColumns"		=>	array(
									array("mData"	=> "id"),
									array("mData"	=> "title"),
								),
			"aoColumnDefs"	=>	array(
									array("sTitle"	=> "ID",	"aTargets"=>array(0)),
									array("sTitle"	=> "Titel",	"aTargets"=>array(1)),
								)
		)
);