<?php 
/**
 * $model is a Model_Node
 * $language is the the requested content-language. 
 * if $language is not set, we'll send all the languages
 * $tags is the mapping for model-properties <-> xml-tags
 * 
 * this is the default view, if a nodetemplate doesn't have
 * it's own viewxml
 * 
 */


$node_attributes = array(	$tags['id']				=> $model->id,
							$tags['visible']		=> $model->visible,
							$tags['created_on']		=> $model->created_on,
							$tags['modified_on']	=> $model->modified_on );


$out  = Backend_Dom::wrap($tags['title'], 			Frontend_XML::entities($model->title ));
$out .= Backend_Dom::wrap($tags['description'], 	Frontend_XML::entities($model->description ));


// output all languages if specific language is not set
if (isset($language)) {
	$languages = array($language);
} else {
	$languages = array_keys(Kohana::$config->load('sigi.languages'));
}
foreach ( $languages as $lang) {
	$meta = $model->nodemetaobjects->where('lang','=',$lang)->find();
	if ($meta->loaded()) {
		
		$locale_attributes = array(	$tags['lang']	=> $lang);
		
		$locale_out  = Backend_Dom::wrap($tags['menutitle'],		Frontend_XML::entities($meta->menutitle));
		
		foreach ($model->nodetemplate->get_containers() as $container_name) {
			$container_attributes	 = array(	$tags['name']	=> $container_name);
			$contents		  		 = $model->contents->where('container','=',$container_name)->where('lang','=',$lang)->where('visible','=','1')->order_by('position', 'ASC')->find_all();
			$container_out			 = Frontend_XML::contents($contents, $lang, $tags);
			$locale_out				.= Backend_Dom::wrap($tags['container'], $container_out, $container_attributes);
		}
		$out .= Backend_Dom::wrap($tags['locale'], $locale_out, $locale_attributes);		
	}
}

// the child nodes
$childnodes = $model->children();
$out .= Frontend_XML::nodes($childnodes, $language, $tags);

// wrap it all up
echo Backend_Dom::wrap($tags['node'], $out, $node_attributes);