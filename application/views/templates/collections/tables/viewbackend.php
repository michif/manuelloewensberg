<?
// Include der Properties
$table_properties = include(Kohana::find_file("views/".$model->nodetemplate->viewfolder, "properties"));

$subview=new View($model->nodetemplate->viewfolder."/manipulate");
$subview->model=$model;
echo $subview;


echo '<div class="tabs">';
echo '<ul class="tablinks">';
foreach($model->nodetemplate->get_containers() as $container){
	echo '<li>'.HTML::anchor("#".$container."_tab", $container).'</li>';
}
echo '</ul>';

foreach($model->nodetemplate->get_containers() as $container){
	
	echo '<div id="'.$container.'_tab" class="addmenucontainer tabcontainer" >';
	$modules=$model->nodetemplate->contentmodules->where('container', '=', $container)->find_all();
	
	echo '<div class="tabviewactions filtercontainer"></div>';
	
	if (array_key_exists ( $container ,$table_properties )) {
		echo Backend_Addmenu::content($model->id,$container,$modules, NULL, "content_tables");
	}
	echo '<div class="container_content">';
	if (array_key_exists ( $container ,$table_properties )) {
		echo '<table class="elmcontainer datatable" elmid="'.$model->id.'" container="'.$container.'" lang="'.Session::instance()->get('sigi_language').'" properties="'.htmlentities(json_encode($table_properties[$container])).'" >';
		echo '</table>';
	
	} else {
		$view = new View('errors/errorgui');
		$view->message = "Container " . $container . " has no table properties. Add it to <code>properties.php</code>.";
		echo $view;
	
	}
	
	echo '</div>';
	echo '</div>';
	
}
echo '</div>';// Tabs
?>

