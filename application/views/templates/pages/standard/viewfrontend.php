<? $head = new View("templates/pages/elements/head");
    $head -> description = $description;
    $head -> keywords = $keywords;
    $head -> title = $title;
    $head -> generator = $generator;
    echo $head;
?>

</head>
<body>
    <?= isset($preview) ? $preview : ''; ?>



    <div class="sticky show-for-medium-up">

            <nav id="mainnavi" class="top-bar" data-topbar role="navigation">





              <h1 class="text-center fncy-custom-close">Manuel Löwensberg</h1>

     
               <div class="row show-for-medium-up">
                    <div class="large-12 medium-12  columns" role="main">
                        <? $subview = new View("templates/navigations/mainnavi");
                            echo $subview;
                        ?>
                    </div>

  <!--

  <section class=" top-bar-section">
        <h1 class="title middle">Manuel Löwensberg</h1>
      </section>

              <h1 class="text-center">Manuel Löwensberg</h1>-->

              <!--  <div class="row show-for-medium-up">
                    <div class="large-12 medium-12  columns" role="main">
                        <? $subview = new View("templates/navigations/mainnavi");
                            echo $subview;
                        ?>
                    </div>-->
                </div>

            </nav>
    </div>
    
    
    


    
    




  <div id="content" class="row show-for-medium-up">
        <div class="large-12 medium-12 columns" role="main">
            <div class="content">
                <?= isset($content) ? $content : ''; ?>
            </div>
        </div>
    </div>



<div class="off-canvas-wrap show-for-small-only" data-offcanvas>
  <div class="inner-wrap ">
       <div class=" sticky  show-for-small-only">

    <nav class="tab-bar" data-offcanvas>
      <section class="left-small">
        <a class="left-off-canvas-toggle menu-icon"><span></span></a>
      </section>

      <section class="middle tab-bar-section">
        <h1 class="title">Manuel Löwensberg</h1>
      </section>
    </nav>
    </div>



    <aside class="left-off-canvas-menu">
      <ul class="off-canvas-list">
          
  <? $subview = new View("templates/navigations/mainnavi");
                            echo $subview;
                        ?>          
       
      </ul>
    </aside>

   

    <section class="main-section full-height">
      
      <div id="content" class="row ">
        <div class="large-12 medium-12 columns" role="main">
            <div class="content">
                <?= isset($content) ? $content : ''; ?>
            </div>
        </div>
    </div>
      
      
    </section>

  <a class="exit-off-canvas"></a>

  </div>
</div>



<!--

 <div class="off-canvas-wrap">
            <div class="inner-wrap">
                <div class="header">
                    <nav class="tab-bar" data-offcanvas>
                        <section class="left-small"> <a class="left-off-canvas-toggle menu-icon"><span></span></a>
                        </section>
                        <section class="middle tab-bar-section">
                               <h1 class="title">Manuel Löwensberg</h1>
                        </section>
                    </nav>
                </div>
                <aside class="left-off-canvas-menu">
                    <ul class="off-canvas-list">
                        <li>
                            <label>Navigation</label>
                        </li>
                       <? $subview = new View("templates/navigations/mainnavi");
                            echo $subview;
                        ?>   
                    </ul>
                </aside>
                <article class="small-12 columns">
                    <div id="content" class="row ">
                        <div class="large-12 medium-12 columns" role="main">
                         <div class="content">
                            <?= isset($content) ? $content : ''; ?>
                    </div>
                </div>
               </div>
               </article> 
                <a class="exit-off-canvas"></a>

                
            </div>
        </div>

-->
    
    <?

$foot=new View("templates/pages/elements/foot");
echo $foot;

echo HTML::script('media/js/sigifront.js').PHP_EOL;

echo new View("templates/pages/elements/tracking");
    ?>
</body>
</html>