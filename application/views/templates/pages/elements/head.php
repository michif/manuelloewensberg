<!DOCTYPE html>
<html class="<?= (isset($html_class))?$html_class:''?> no-js" >
<head>
<meta charset="UTF-8">
<meta name="description" content="<?= (isset($description))?$description:'' ?>" />
<meta name="keywords" content="<?= (isset($keywords))?$keywords:'' ?>" />
<meta name="generator" content="<?= (isset($generator))?$generator:'' ?>" />
<meta name="viewport" content="<?= (isset($viewport))?$viewport:'width=device-width initial-scale=1 user-scalable=no' ?>" /><!-- initial-scale=.73-->
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="1 day" />

<!-- Preview Images for FB-->
<meta property="og:image" content="<?=URL::base(true)?>media/img/preview.jpg" />
<meta property="og:title" content="<?= (isset($title))?$title:'' ?>" />
<meta property="og:description" content="<?= (isset($description))?$description:'' ?>" />
<meta property="og:url" content="http://www.url.ch" />


<link rel="image_src" href="<?=URL::base(true)?>media/img/preview.jpg" />


<title><?= (isset($title))?$title:'' ?></title>
<link rel="icon" type="image/x-icon" href="<?=URL::base(true)?>media/img/favicon.ico" />
<link rel="shortcut icon" type="image/x-icon" href="<?=URL::base(true)?>media/img/favicon.ico" />

<script>
	var baseurl="<?=URL::base(true)?>";
</script>

<?
            echo HTML::style('media/js/fancybox/jquery.fancybox.css?v=2.1.5', array("media"=>"screen")).PHP_EOL;

	echo HTML::style('media/styles/css/screen.css', array("media"=>"all")).PHP_EOL;
	echo HTML::style('media/styles/css/print.css', array("media"=>"print")).PHP_EOL;
    
    echo HTML::style('bower_components/slick-carousel/slick/slick.css').PHP_EOL;
            echo HTML::style('bower_components/slick-carousel/slick/slick-theme.css').PHP_EOL;
    
        echo HTML::style('media/js/photostack/jquery.Photostack.css').PHP_EOL;
    
    
	
	
	echo HTML::script('bower_components/modernizr/modernizr.js')."\n";
    
    
    
    
?>