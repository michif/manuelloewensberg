<?
echo Backend_Content::open($model);
echo Backend_Content::actioncontainer($model);
echo Backend_Content::content_open($model);

echo Backend_Editable::input_heading("content", $model, "title", "", Backend_Editable::RELOAD_ELEMENT);
echo Backend_Editable::input_heading("content", $model, "subtitle", "", "", NULL, NULL, "h2");
echo Backend_Editable::image("content", $model, "url", "Id-Bild");
echo Backend_Editable::input_redactor("content", $model, "lead", "Lead");
echo Backend_Editable::input_redactor("content", $model, "text" ,"Text");
echo Backend_Editable::date("content", $model, "date", "Datum");
echo Backend_Content::content_close();

echo Backend_Content::mediacontainer($model, "image", "Bilder", array("image"));
echo Backend_Content::mediacontainer($model, "file", "PDF", array("application/pdf"));
echo Backend_Content::mediacontainer($model, "video", "Videos", array("video"));
echo Backend_Content::close();

