<?php 
/**
 * $model is a Model_Node
 * $language is the the requested content-language. 
 * $tags is the mapping for model-properties <-> xml-tags
 * 
 * this is the default view, if a contentelement doesn't
 * have it's own viewxml.
 * 
 */

$content_attributes = array(	$tags["id"]				=> $model->id,
								$tags["visible"]		=> $model->visible);

$out  = Backend_Dom::wrap($tags['title'], 		Frontend_XML::entities($model->title));


// CONTENTLINKS INCOMING / OUTGOING
$out .= Frontend_XML::contentlinks($model->contentlinks->find_all(), $model->contentlinksonme->find_all(), $tags);

// wrap everything up and send it back
echo Backend_Dom::wrap($tags['content'], $out, $content_attributes);