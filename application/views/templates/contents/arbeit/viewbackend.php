<?
$kundenarray=$model->node->contents->where("container", "=", "kunden")->order_by("title")->find_all()->as_array("id", "title");


$kundentitles = array();
foreach($model->contentlinks->where("container", "=", "kunden")->find_all() as $elm){
	$kundentitles[] = $elm->linkedcontent->title;
}

$tagarray=$model->node->contents->where("container", "=", "tags")->order_by("title")->find_all()->as_array("id", "title");

$tagtitles = array();
foreach($model->contentlinks->where("container", "=", "tags")->find_all() as $elm){
	$tagtitles[] = $elm->linkedcontent->title;
}

$tagtitles2 = array();
foreach($model->contentlinks->where("container", "=", "tags2")->find_all() as $elm){
	$tagtitles2[] = $elm->linkedcontent->title;
}

echo Backend_Content::open($model,"","", "div","templates/contents/arbeit/viewbackend");
echo Backend_Content::actioncontainer($model);
echo Backend_Content::content_open($model);
echo Backend_Editable::input_heading("content", $model, "title", NULL, "element");
echo Backend_Editable::input_heading("content", $model, "subtitle", "", "", NULL, NULL, "h2");
echo Backend_Editable::image("content", $model, "url", "Id-Bild");
echo Backend_Editable::input_redactor("content", $model, "lead", "Lead");
echo Backend_Editable::input_redactor("content", $model, "text" ,"Text");
echo Backend_Editable::date("content", $model, "date", "Datum");


echo Backend_Editable::multiselect("content", $model, "contentlinks", "Kundenlinks", "", NULL, NULL, $kundenarray, "kunden",  NULL, $kundentitles);
echo Backend_Editable::multiselect("content", $model, "contentlinks", "Taglinks1", "", NULL, NULL, $tagarray, "tags",  NULL, $tagtitles);
echo Backend_Editable::multiselect("content", $model, "contentlinks", "Taglinks2", "", NULL, NULL, $tagarray, "tags2",  NULL, $tagtitles2);


echo Backend_Content::content_close();
echo Backend_Content::mediacontainer($model);
echo Backend_Content::close("div");