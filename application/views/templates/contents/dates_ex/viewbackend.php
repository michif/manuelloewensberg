<?
echo Backend_Content::open($model);
echo Backend_Content::actioncontainer($model);
echo Backend_Content::content_open($model);

echo Backend_Editable::input_heading("content", $model, 'title', "", Backend_Editable::RELOAD_ELEMENT);
$collarray=ORM::factory('Node')->where("type", "=", "collection")->find_all()->as_array("id", "title");

echo Backend_Editable::select("content", $model, "var1", "Collection", Backend_Editable::RELOAD_ELEMENT, NULL, NULL, $collarray);

if($model->var1!=""){
	$collection=ORM::factory('Node', $model->var1);
	
	echo Backend_Editable::select("content", $model, "var2", "Container", Backend_Editable::RELOAD_ELEMENT, NULL, NULL, $collection->nodetemplate->get_containers_select());
	echo '<hr />';
	echo "Vorschau:<br>";

	echo $model->var2;
	
	
	if($model->var2!=""){
		foreach($collection->contents->where("container", "=", $model->var2)->order_by('date', 'asc')->where("lang", "=", Session::instance()->get('sigi_language'))->find_all() as $cont){
			echo '<div class="">';
			echo ($cont->title!=""?$cont->title:'(no title)');
			echo '</div>';
		}
	}
}

echo Backend_Content::content_close();
echo Backend_Content::close();
