<?
echo Backend_Content::open($model);
echo Backend_Content::actioncontainer($model);
echo Backend_Content::content_open($model);

echo Backend_Editable::input_heading("content", $model, "title", "", Backend_Editable::RELOAD_ELEMENT);
echo Backend_Editable::input_heading("content", $model, "subtitle", "", "", NULL, NULL, "h2");
echo Backend_Editable::image("content", $model, "url", "Id-Bild");
echo Backend_Editable::input_redactor("content", $model, "lead", "Lead");
//echo Backend_Editable::input_redactor("content", $model, "text" ,"Text");
echo Backend_Editable::input_code("content", $model, "var1" ,"var1");

echo Backend_Editable::date("content", $model, "date", "Datum");
echo Backend_Content::content_close();
echo Backend_Content::close();

