<?php 

/**
 * overwrite this array in your contentelement/xmltags
 * if you want to have different tag-names for the
 * model-properties in the xml-output
 */

return array(
		"id"				=> "id",
		"shortname"			=> "shortname",
		"node_id"			=> "node_id",
		"contentmodule_id"	=> "contentmodule_id",
		"parent_id"			=> "parent_id",
		"visible"			=> "visible",
		"lang"				=> "lang",
		"container"			=> "container",
		"type"				=> "type",
		"title"				=> "title",
		"subtitle"			=> "subtitle",
		"lead"				=> "lead",
		"text"				=> "text",
		"tags"				=> "tags",
		"code"				=> "code",
		"url"				=> "id_image",		// <- hurray we have a special tag!!
		"date"				=> "date",
		"var1"				=> "var1",
		"var2"				=> "var2",
		"var3"				=> "var3",
		"var4"				=> "var4",
		"var5"				=> "var5",
		"var6"				=> "var6",
		"var7"				=> "var7",
		"var8"				=> "var8",
		"var9"				=> "var9",
		"var10"				=> "var10",
		"var11"				=> "var11",
		"var12"				=> "var12",
		"var13"				=> "var13",
		"var14"				=> "var14",
		"var15"				=> "var15",
		"vars"				=> "vars",
		"position"			=> "position",
		"created_on"		=> "created_on",
		"created_by"		=> "created_by",
		"modified_on"		=> "modified_on",
		"modified_by"		=> "modified_by",
		"contents"			=> "contents",
		"content"			=> "content",
		"contentlinks"		=> "contentlinks",
		"contentlink"		=> "contentlink",
		"incoming"			=> "incoming",
		"outgoing"			=> "outgoing",
		"linked_id"			=> "linked_id",
		"linked_title"		=> "linked_title",
		"medialinks"		=> "medialinks",
		"media"				=> "media",
		"mimetype"			=> "mimetype",
		"bytes"				=> "bytes",
		"filename"			=> "filename",
		"hash"				=> "hash",
		"filetype"			=> "filetype",
		"fileurl"			=> "url"
		);