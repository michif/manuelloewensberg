# How To Install Sigi3, using git-repo and git-ftp, 29.1.2013:

* Pull this repo to your local machine
* Rename the file to .gitignore using rename_gitignore.sh
* run the installer on your local machine
* run bower install to install zurb and jquery and stuff
* run grunt to check if everything works as expected
* update the git-ftp-vars.txt with your webhosting parameters
* prepare your online website, create domain, create database, enable ftp and stuff
* upload the contents, using the git-ftp-init.sh
* Run the installer script on your Server
* Your website should now be up and running online
* If you make changes now locally, you can use git-ftp-push.sh to push them to your online website

YEAH!!!


Components used by Sigi CMS

# Development:
## Bower:
https://github.com/bower/bower
type „bower update“ to install latest components as defined in bower.json

## Grunt:
http://gruntjs.com
type grunt to start sass css creation

## Git-FTP:
Is our deployment tool
https://github.com/git-ftp/git-ftp

## Zurb Foundation
http://foundation.zurb.com
We use zurb-foundation as our main frontend framework in the lib-sass version

## Bourbon:
For sass-mixins we use the bourbon classes
https://github.com/thoughtbot/bourbon
go to media/styles/scss/ and type bourbon update to keep this up to date

## Node modules used for grunt:
* grunt
* grunt-contrib-watch
* grunt-sass
* node-sass
use npm update node-sass to get the latest

# Components:
## el-finder for file management
http://elfinder.org

## Redactor 
As our main Text-editor
http://imperavi.com/redactor/

