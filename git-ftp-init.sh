#! /bin/bash
BASEDIR=$(dirname $0)
cd $BASEDIR
. git-ftp-vars.txt
cp .git-ftp-ignore-init .git-ftp-ignore
git ftp init --user $USER --passwd $PASSWORD $URL
rm .git-ftp-ignore .git-ftp-ignore-init
