#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
source git-ftp-vars.txt
cp .git-ftp-ignore-push .git-ftp-ignore
git ftp push --user $USER --passwd $PASSWORD $URL
rm .git-ftp-ignore